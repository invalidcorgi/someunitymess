﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Drop : MonoBehaviour
{
    public Image eq1;
    public Image eq2;
    public Image eq3;
    public Image eq4;
    public Image eq5;
    public Image eq6;
    private List<Image> _images;
    private Material _defaultMaterial;
    public GameObject cubePrefab;
    
    // Start is called before the first frame update
    void Start()
    {
        _images = new List<Image>
        {
            eq6,
            eq5,
            eq4,
            eq3,
            eq2,
            eq1
        };

        _defaultMaterial = eq1.material;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            var image = _images.FirstOrDefault(img => img.color != Color.white);

            if(image == default)
                return;
            
            var newCube = Instantiate(cubePrefab);
            newCube.GetComponent<Renderer>().material.color = image.color;
            newCube.transform.position = transform.position;
            image.color = Color.white;
        }
    }
}
