﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public GameObject NPCPanel;

    public Text NPCText;

    public Text Button1;

    public Text Button2;
    
    public GameObject slidingDoor;
    public float slidingSpeed;
    private bool started = false;
    private bool ended = false;
    // Start is called before the first frame update
    void Start()
    {
        ResetPanel();
    }

    // Update is called once per frame
    void Update()
    {
        if(ended || !started)
            return;

        var startingPosition = slidingDoor.transform.position;
        
        startingPosition.x -= slidingSpeed * Time.deltaTime;
        if (startingPosition.x < -20)
        {
            startingPosition.x = -20;
            ended = true;
        }

        slidingDoor.transform.position = startingPosition;
    }

    public void CloseNPCPanel()
    {
        ResetPanel();
        NPCPanel.SetActive(false);
    }

    public void IFeelGood()
    {
        switch (NPCText.text)
        {
            case "Cadence, czy zabiłaś już Cryptodancera?":
                NPCText.text = "Ciekawe, a kogo zobaczyłaś po pokonaniu go?";
                Button1.text = "Moją mamę Melody";
                Button2.text = "Twoją starą";
                break;
            case "Ciekawe, a kogo zobaczyłaś po pokonaniu go?":
                NPCText.text = "Dobrze, dobrze, powiedz mi kto jest najdziwniejszą  kreaturą w naszym świecie.";
                Button1.text = "Coda";
                Button2.text = "Ty";
                break;
            case "Dobrze, dobrze, powiedz mi kto jest najdziwniejszą  kreaturą w naszym świecie.":
                started = true;
                CloseNPCPanel();
                break;
        }
        
    }

    private void ResetPanel()
    {
        NPCText.text = "Cadence, czy zabiłaś już Cryptodancera?";
        Button1.text = "Tak";
        Button2.text = "Skąd";
    }
}
