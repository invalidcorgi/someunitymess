﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HingedDoors : MonoBehaviour
{
    public float slidingSpeed;
    public float maxRotation;
    private bool started = false;
    private bool ended = false;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(ended || !started)
            return;

        var startingRotation = transform.rotation;
        
        Debug.Log($"x: {startingRotation.x} y: {startingRotation.y} z: {startingRotation.z}");
        
        startingRotation.y += slidingSpeed * Time.deltaTime;
        if (Math.Abs(startingRotation.y - maxRotation) < 0.001)
        {
            startingRotation.y = maxRotation;
            ended = true;
        }

        transform.rotation = startingRotation;
    }

    private void OnCollisionEnter(Collision other)
    {
        started = true;
    }
}
