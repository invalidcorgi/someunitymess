﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class PickUp : MonoBehaviour
{
    private GameObject _player;

    private List<Image> _images;
    // Start is called before the first frame update
    void Start()
    {
        _player = GameObject.Find("Player");
        _images = new List<Image>();
        for (int i = 1; i <= 6; i++)
        {
            _images.Add(GameObject.Find($"Eq{i}").GetComponent<Image>());
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space) == false)
            return;
        
        var myPosition = gameObject.transform.position;
        var playerPosition = _player.transform.position;

        if (Mathf.Sqrt(Mathf.Pow((playerPosition.x) - myPosition.x, 2)
                       + Mathf.Pow((playerPosition.y) - myPosition.y, 2)
                       + Mathf.Pow((playerPosition.z) - myPosition.z, 2)) < 1)
        {
            var image = _images.First(img => img.color == Color.white);
            image.color = GetComponent<Renderer>().material.color;
            Destroy(gameObject);
        }
    }
}
