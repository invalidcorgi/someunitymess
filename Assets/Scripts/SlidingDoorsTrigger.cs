﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlidingDoorsTrigger : MonoBehaviour
{
    public GameObject slidingDoor;
    public float slidingSpeed;
    private bool started = false;
    private bool ended = false;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(ended || !started)
            return;

        var startingPosition = slidingDoor.transform.position;
        
        startingPosition.x -= slidingSpeed * Time.deltaTime;
        if (startingPosition.x < -20)
        {
            startingPosition.x = -20;
            ended = true;
        }

        slidingDoor.transform.position = startingPosition;
    }

    private void OnCollisionEnter(Collision other)
    {
        started = true;
    }
}
